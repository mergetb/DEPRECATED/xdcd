package main

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/xdcd/api"
)

func (x *XdcdServer) ClearTunnelData(
	ctx context.Context, req *xdcd.ClearTunnelDataRequest,
) (*xdcd.ClearTunnelDataResponse, error) {

	log.Info("Got clear tunnel data request: %#v", req)

	err := ClearTunnelData()
	if err != nil {
		return nil, fmt.Errorf("Error: %s", err)
	}

	return &xdcd.ClearTunnelDataResponse{}, nil
}

func (x *XdcdServer) AddUsers(
	ctx context.Context, req *xdcd.AddUsersRequest,
) (*xdcd.AddUsersResponse, error) {

	log.Info("add users request")

	err := AddUsers(req.Users)
	if err != nil {
		return nil, fmt.Errorf("add users: %w", err)
	}

	return &xdcd.AddUsersResponse{}, nil
}

func (x *XdcdServer) DeleteUsers(
	ctx context.Context, req *xdcd.DeleteUsersRequest,
) (*xdcd.DeleteUsersResponse, error) {

	log.Info("delete users request")

	err := DeleteUsers(req.Users)

	if err != nil {
		return nil, fmt.Errorf("del users: %w", err)
	}

	return &xdcd.DeleteUsersResponse{}, nil
}
func (x *XdcdServer) InitHost(
	ctx context.Context, req *xdcd.InitHostRequest,
) (*xdcd.InitHostResponse, error) {

	log.Info("init host")

	err := InitHost()
	if err != nil {
		return nil, fmt.Errorf("init error: %w", err)
	}

	return &xdcd.InitHostResponse{}, nil
}
