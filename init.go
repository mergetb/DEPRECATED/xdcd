package main

import (
	"fmt"
	"os"
	"os/exec"
	"path"

	"io/ioutil"

	log "github.com/sirupsen/logrus"
)

func InitHost() error {

	// todo:
	// setup sshd_config and start
	// password-less sudo
	// possibly setup /etc/resolv.conf to be read/write?

	log.Info("init'ing the host")

	err := configSshd()
	if err != nil {
		return err
	}

	err = configSudo()
	if err != nil {
		return err
	}

	return nil
}

func configSudo() error {

	// A better idea is to make a merge group and have all users in that. Alas.

	p := path.Join("/", "etc", "sudoers.d")

	if _, err := os.Stat(p); os.IsNotExist(err) {
		return nil // so sudoers, don't bother.
	}

	conf := "%sudo  ALL=(ALL)       NOPASSWD: ALL"

	p = path.Join(p, "10-merge") // file cannot have . in it or end in ~

	err := ioutil.WriteFile(p, []byte(conf), 0600)
	if err != nil {
		return err
	}

	return nil
}

func configSshd() error {

	// # setup ssh certs
	// echo 'TrustedUserCAKeys /etc/step-ca/data/certs/ssh_user_ca_key.pub' >> /etc/ssh/sshd_config
	// echo 'HostKey /etc/ssh/merge/merge_key' >> /etc/ssh/sshd_config
	// echo 'HostCertificate /etc/ssh/merge/merge_key-cert.pub' >> /etc/ssh/sshd_config
	// service ssh reload
	//
	// Having the paths passed to xdcd might be nice....
	//
	// NOTE: this code assumes that the mergefs and cred reconciler have placed the sshd credentials.
	//
	conf := `
#
# setup merge generated ssh certs and keys
#
TrustedUserCAKeys /etc/step-ca/data/certs/ssh_user_ca_key.pub
HostKey /etc/ssh/auth/merge_key
HostCertificate /etc/ssh/auth/merge_key-cert.pub
`
	p := path.Join("/", "etc", "ssh", "sshd_config.d", "10-merge.conf")
	err := ioutil.WriteFile(p, []byte(conf), 0600)
	if err != nil {
		return err
	}

	// restart sshd

	_, err = exec.LookPath("service")
	if err == nil {
		c := exec.Command("service", "ssh", "restart")
		_, err = c.CombinedOutput()
		if err != nil {
			return err
		}
	} else {
		// just kill and restart by hand. (Alpine container)
		_ = exec.Command("pkill", "sshd").Run()
		err = exec.Command("/usr/sbin/sshd", "-e").Run()
		if err != nil {
			return fmt.Errorf("error starting sshd: %w", err)
		}
	}
	return nil
}
