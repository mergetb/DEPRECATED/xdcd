VERSION = $(shell git describe --always --long --dirty)
LDFLAGS = "-X main.Version=$(VERSION)"

XDCDSRC = main.go grpc.go xdcd.go users.go init.go
XDCDCMDSRC= xdcdcmd/main.go

all: build/xdcd build/xdcdcmd

build/xdcd: ${XDCDSRC} api/xdcd.pb.go | build
	CGO_ENABLED=0 go build -ldflags=${LDFLAGS} -o $@ ${XDCDSRC}

build/xdcdcmd: ${XDCDCMDSRC} api/xdcd.pb.go ./build/xdcd | build
	CGO_ENABLED=0 go build -ldflags=${LDFLAGS} -o $@ ${XDCDCMDSRC}

api/xdcd.pb.go: api/xdcd.proto | .proto
	protoc -I . -I .proto -I 3p \
		--go_out=plugins=grpc:api \
		--go_opt=module=gitlab.com/mergetb/portal/xdcd/api \
		--govalidators_out=. \
		--govalidators_opt=paths=source_relative \
		api/xdcd.proto

# pull external protobug definition that we use. This may be a bad idea.
.proto:
	mkdir -p .proto/portal/services/api .proto/site/api .proto/xir/v0.3
	curl -sL https://gitlab.com/mergetb/xir/-/raw/virt/v0.3/core.proto -o .proto/xir/v0.3/core.proto
	curl -sL https://gitlab.com/mergetb/site/-/raw/virt/api/shared.proto -o .proto/site/api/shared.proto
	curl -sL https://gitlab.com/mergetb/portal/services/-/raw/gtl-dev/api/workspace_types.proto \
		-o .proto/portal/services/api/workspace_types.proto

build:
	$(QUIET) mkdir -p build

.PHONY: clean
clean:
	rm -rf build
	find api -name "*.pb.go" -delete

