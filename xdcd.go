package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"
)

func ClearTunnelData() error {

	// default resolv.conf on a k8s portal xdc
	const resolv string = `
search xdc.svc.cluster.local svc.cluster.local cluster.local
nameserver 10.2.0.2
options ndots:5
`
	log.Infof("Clearing tunnel data if it exists.")

	// If there's an existing cached resolv.conf use it
	if _, err := os.Stat("/etc/xdc/resolv"); err == nil {

		src, err := os.Open("/etc/xdc/resolv")
		if err != nil {
			return fmt.Errorf("open: %s", err)
		}
		defer src.Close()

		dst, err := os.Create("/etc/resolv.conf")
		if err != nil {
			return fmt.Errorf("create: %s", err)
		}
		defer dst.Close()

		_, err = io.Copy(dst, src)
		if err != nil {
			return fmt.Errorf("copy: %s", err)
		}
	} else {
		// just write the default resolv.
		err = ioutil.WriteFile("/etc/resolv.conf", []byte(resolv), 0644)
		if err != nil {
			return fmt.Errorf("write file: %s", err)
		}
	}

	log.Infof("Restored /etc/resolv.conf")

	files := []string{"rid", "eid", "pid", "resolv"}
	for _, f := range files {
		err := os.Remove("/etc/xdc/" + f)
		if err != nil {
			return fmt.Errorf("Remove %s: %s", f, err)
		}
		log.Infof("Removed /etc/xdc/" + f)
	}

	return nil
}
