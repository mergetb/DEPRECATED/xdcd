package main

import (
	"flag"
	"net"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/portal/xdcd/api"
	"google.golang.org/grpc"
)

var Version = "undefined"

type XdcdServer struct {
}

func main() {
	listen := flag.String("listen", "0.0.0.0:6000", "Where and how to listen for connections")
	logLevel := flag.String("loglevel", "info", "Log level")
	flag.Parse()

	lvl, err := log.ParseLevel(*logLevel)
	if err != nil {
		log.SetLevel(log.InfoLevel)
		log.Errorf("bad 'loglevel' argument: %s. ignoring and setting to Info level", logLevel)
	} else {
		log.Infof("setting log level to %s", *logLevel)
		log.SetLevel(lvl)
	}

	l, err := net.Listen("tcp", *listen)
	if err != nil {
		log.Fatalf("failed to listen: %#v", l)
	}

	log.Infof("xdcd version %s", Version)
	log.Infof("listening on tcp://%s", *listen)

	grpcSrv := grpc.NewServer()
	xdcd.RegisterXdcdServer(grpcSrv, &XdcdServer{})

	err = grpcSrv.Serve(l)
	if err != nil {
		log.Fatalf("serve error: %#v", err)
	}

	os.Exit(0)
}
