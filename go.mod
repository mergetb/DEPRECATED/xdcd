module gitlab.com/mergetb/portal/xdcd

go 1.13

require (
	github.com/golang/protobuf v1.4.3
	github.com/konsorten/go-windows-terminal-sequences v1.0.3 // indirect
	github.com/mwitkow/go-proto-validators v0.3.2
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/cobra v1.0.0
	gitlab.com/mergetb/portal/services v0.5.18-0.20210209195627-ca295d862087
	google.golang.org/grpc v1.33.1
	google.golang.org/protobuf v1.25.0
)
